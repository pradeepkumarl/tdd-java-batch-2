package com.training.model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserTest {

	@AfterEach
	void afterEach() {
		System.out.println("Will be called after each method::");
	}

	@BeforeEach
	void beforeEach() {
		System.out.println("Will be called before each method::");
	}
	
	@AfterAll
	static void afterAll() {
		System.out.println("Will be called after all the test cases::");
	}

	@BeforeAll
	static void beforeAll() {
		System.out.println("Will be called before all the test cases::");
	}

	
	@Test
	void  testConstructor() {
		//execute
		User user = new User(12, "Ramesh", 18);
		//assert the behaviour
		Assertions.assertNotNull(user);
		Assertions.assertEquals(12, user.getId());
		Assertions.assertEquals("Ramesh", user.getName());
		Assertions.assertEquals(18, user.getAge());
	}
	
	@Test
	void  testSetter() {
		//execute
		User user = new User(12, "Ramesh", 18);
		//assert the behaviour
		user.setAge(33);
		Assertions.assertNotNull(user);
		Assertions.assertEquals(33, user.getAge());
	}

}
