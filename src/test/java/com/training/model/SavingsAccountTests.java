package com.training.model;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.training.dao.AccountDAO;
import com.training.exception.InsufficientBalanceException;

@ExtendWith(MockitoExtension.class) 
public class SavingsAccountTests {

	
	@Mock
	private AccountDAO accountDAO;
	
	@InjectMocks
	private SavingsAccount savingsAccount;

	@BeforeEach
	void initializeSavingsAccount() {
	//	savingsAccount = new SavingsAccount("Ravish", 5000);
	}

	@Test
	void testSavingsAccountConstructor() {
		SavingsAccount savingsAccount = new SavingsAccount("Ravish", 5000);

		// assertions
		Assertions.assertNotNull(savingsAccount);
		Assertions.assertEquals("Ravish", savingsAccount.getName());
		Assertions.assertEquals(5000, savingsAccount.getBalance());
	}

	@Test
	void withdrawPositiveTest() {
		savingsAccount.deposit(5000);
		Assertions.assertDoesNotThrow(() ->{
			double amount = savingsAccount.withdraw(2000);
			Assertions.assertEquals(2000, amount);
		});
		
	}

	@Test
	void withdrawNegativeTest() {
		Assertions.assertThrows(InsufficientBalanceException.class, () ->{
			savingsAccount.withdraw(8000);
		});
	}
	
	@Test
	void testDeposit() {
		savingsAccount.deposit(4000);
		Assertions.assertTrue(savingsAccount.getBalance() == 4000);
	}
	
	@Test
	void testDepositWithDependencies() {
		//expect that the dependencies are met
		Mockito.when(accountDAO.deposit(anyDouble())).thenReturn(4000d);
		
		savingsAccount.deposit(4000);
		
		Assertions.assertTrue(savingsAccount.getBalance() == 4000);
		
		verify(accountDAO, times(1)).deposit(4000);
	}
}
