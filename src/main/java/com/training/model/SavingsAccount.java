package com.training.model;

import com.training.dao.AccountDAO;
import com.training.exception.InsufficientBalanceException;

public class SavingsAccount {

	private String name;
	private double accountBalance;
	private AccountDAO accountDAO;

	private SavingsAccount() {}
	public SavingsAccount(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}
	public SavingsAccount(String name, double initialAccountBalance) {
		this.name = name;
		this.accountBalance = initialAccountBalance;
	}

	public String getName() {
		return this.name;
	}

	public double getBalance() {
		return this.accountBalance;
	}

	public double withdraw(double amountToBeWithdrawn) throws InsufficientBalanceException {
		if (this.accountBalance - amountToBeWithdrawn > 0) {
			return amountToBeWithdrawn;
		} 
		throw new InsufficientBalanceException("insufficient funds");
	}

	public void deposit(double amount) {
		this.accountBalance = this.accountDAO.deposit(amount);
	}

}
